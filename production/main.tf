module "sporchia-module" {
  source = "gitlab.com/gl-demo-ultimate-msporchia1/terraform-sample-module-1/local"
  version = "0.0.2"

  env_name = var.env_name
  aws_region = var.aws_region
}